
module omni.client {
    requires org.lwjgl;
    requires org.lwjgl.glfw;
    requires org.lwjgl.opengl;
    requires java.logging;
    requires static lombok;
    requires devil.util;
    requires osgi.core;
    requires omni.module;
    requires omni.resource;
    requires trivial.json;
    exports net.angle.omniclient.api;
    exports net.angle.omniclient.impl;
}