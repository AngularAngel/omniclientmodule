package net.angle.omniclient.api;

import com.samrj.devil.game.GameWindow;
import com.samrj.devil.gl.DGL;
import com.samrj.devil.gui.DUI;
import com.samrj.devil.math.Vec2i;
import net.angle.omniresource.api.ResourceManager;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.system.APIUtil;

/**
 *
 * @author angle
 */
public interface Client {
    public default void run(Screen titleScreen) {
        try
        {
            this.preInit();

            GameWindow.onInit(() -> {
                DGL.init();
                DUI.init();
                
                this.init();
                
                changeScreen(titleScreen);
                
                GameWindow.onResize(this::resize);
                GameWindow.onMouseMoved(this::mouseMoved);
                GameWindow.onMouseButton(this::mouseButton);
                GameWindow.onMouseScroll(this::mouseScroll);
                GameWindow.onKey(this::key);
                GameWindow.onStep(this::step);
                GameWindow.afterInput(this::afterInput);
                GameWindow.onRender(this::render);
                GameWindow.onCharacter(DUI::character);
            });
            
            GameWindow.onDestroy(this::destroy);
            
            setRunning(true);
            GameWindow.run();
            
        } catch (Throwable t) {
            t.printStackTrace();
            APIUtil.DEBUG_STREAM.close(); //Prevent LWJGL leak message spam.
            System.exit(-1);
        }
        setRunning(false);
    }
    
    public Vec2i getResolution();
    public void setRunning(boolean running);
    public boolean isRunning();
    public String getTitle();
    public Screen getScreen();
    public ResourceManager getResourceManager();
    public default void preInit()  {
        Thread.currentThread().setName("Client Thread");
        GameWindow.setDebug(true);
            
        //OpenGL context should be forward-compatible, i.e. one where all
        //functionality deprecated in the requested version of OpenGL is
        //removed. In the core profile, immediate mode OpenGL is deprecated.
        GameWindow.hint(GLFW.GLFW_OPENGL_FORWARD_COMPAT, GLFW.GLFW_TRUE);
        GameWindow.hint(GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_CORE_PROFILE);

        //OpenGL 3.2 gives good access to most modern features, and is
        //supported by most hardware.
        GameWindow.hint(GLFW.GLFW_CONTEXT_VERSION_MAJOR, 3);
        GameWindow.hint(GLFW.GLFW_CONTEXT_VERSION_MINOR, 3);
        GameWindow.hint(GLFW.GLFW_DEPTH_BITS, 0);

        GameWindow.setFullscreen(false);
        GameWindow.setTitle(getTitle());
    }
    public void init();
    
    public default void mouseMoved(float x, float y) {
        getScreen().mouseMoved(x, y);
        DUI.mouseMoved(x, y);
    }

    public default void mouseButton(int button, int action, int mods) {
        getScreen().mouseButton(button, action, mods);
        DUI.mouseButton(button, action, mods);
    }
    
    public default void mouseScroll(float start, float end) {
        getScreen().mouseScroll(start, end);
        DUI.mouseScroll(start, end);
    }
    
    public default void key(int key, int action, int mods) {
        getScreen().key(key, action, mods);
        DUI.key(key, action, mods);
    }
    
    public default void resize(int width, int height) {
        getScreen().resize(width, height);
    }
    
    public default void step(float dt) {
        getScreen().step(dt);
    }
    
    public default void afterInput() {
        getScreen().afterInput();
    }
    
    public default void render() {
        getScreen().render();
    }
    
    public default void changeScreen(Screen oldScreen, Screen newScreen) {
        if (getScreen() == oldScreen)
            changeScreen(newScreen);
    }
    public void changeScreen(Screen newScreen);
    public void destroy(Boolean crashed);
}