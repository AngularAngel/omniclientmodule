package net.angle.omniclient.api;

import com.samrj.devil.game.GameWindow;
import com.samrj.devil.gui.Text;
import com.samrj.devil.math.Vec2i;

/**
 *
 * @author angle
 */
public interface MenuScreen<T extends Client> extends Screen<T> {
    
    public default void populateTitleWindow(String title) {
        populateTitleWindow(new Text(title));
    }
    
    public void populateTitleWindow(Text title);
    
    public void populateTitleWindow();
    public void populateMenuWindow();
    
    public void createTitleWindow();
    public void createMenuWindow();
    
    public default void createWindows() {
        createTitleWindow();
        createMenuWindow();
    }
    
    @Override
    public default void init() {
        createWindows();
        
        Vec2i resolution = GameWindow.getResolution();
        
        resize(resolution.x, resolution.y);
    }
}