package net.angle.omniclient.api;

import com.samrj.devil.gui.DUI;
import org.lwjgl.opengl.GL11C;

/**
 *
 * @author angle
 */
public interface Screen<T extends Client> {
    public T getClient();
    public default void init() {};
    public default void mouseMoved(float x, float y) {}

    public default void mouseButton(int button, int action, int mods) {}
    
    public default void mouseScroll(float start, float end) {}
    
    public default void key(int key, int action, int mods) {}
    
    public default void afterInput() {}
    
    public default void resize(int width, int height) {};
    public default void step(float dt) {};
    public default void render() {
        GL11C.glClear(GL11C.GL_COLOR_BUFFER_BIT | GL11C.GL_DEPTH_BUFFER_BIT);
        DUI.render();
    }
    public default void destroy(Boolean crashed) {}
}