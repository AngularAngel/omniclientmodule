package net.angle.omniclient.api;

import java.util.function.Consumer;

/**
 *
 * @author angle
 */
public interface Settings {
    public <T> Setting<T> getSetting(String name, Class<T> type);
    public <T> Setting<T> addSetting(Setting<T> setting);
            
    public static interface Setting<T> {
        public String getName();
        public T getValue();
        public void setCallback(Consumer<T> callback);
        public void setValue(T newValue);
    }
}