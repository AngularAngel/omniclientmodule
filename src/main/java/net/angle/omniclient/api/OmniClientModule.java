package net.angle.omniclient.api;

import com.samrj.devil.gui.Font;
import net.angle.omnimodule.api.OmniModule;
import net.angle.omniresource.api.Resource;
import net.angle.omniresource.api.ResourceManager;
import net.angle.omniresource.api.ResourceType;

/**
 *
 * @author angle
 */
public interface OmniClientModule extends OmniModule {
    public Client createClient(ResourceManager resourceManager, String title);
    public <T extends Client> T setClient(T client);
    public boolean existingClient();
    public Client getClient();
    public void destroyClient();
    public ResourceType<Resource<Font>> getFontResourceType();
    public ResourceType<Resource<Font>> getPropertiedFontResourceType();
}
