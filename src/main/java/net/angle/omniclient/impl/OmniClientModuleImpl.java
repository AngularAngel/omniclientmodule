package net.angle.omniclient.impl;

import com.samrj.devil.gui.Font;
import java.util.Objects;
import net.angle.omniclient.api.Client;
import net.angle.omniclient.api.OmniClientModule;
import net.angle.omnimodule.impl.BasicOmniModule;
import net.angle.omniresource.api.Resource;
import net.angle.omniresource.api.ResourceManager;
import net.angle.omniresource.api.ResourceType;


public class OmniClientModuleImpl extends BasicOmniModule implements OmniClientModule {
    private Client client = null;
    
    public OmniClientModuleImpl() {
        super(OmniClientModule.class);
    }
    
    @Override
    public Client createClient(ResourceManager resourceManager, String title) {
        if (existingClient())
            throw new IllegalStateException("Client already created!");
        return client = new BasicClient(resourceManager, title);
    }

    @Override
    public <T extends Client> T setClient(T client) {
        if (existingClient())
            throw new IllegalStateException("Client already created!");
        this.client = client;
        return client;
    }
    
    @Override
    public boolean existingClient() {
        return Objects.nonNull(client);
    }
    
    @Override
    public Client getClient() {
        if (existingClient())
            return client;
        else throw new IllegalStateException("Cannot get client that has not been created!");
    }
    
    @Override
    public void destroyClient() {
        if (!existingClient())
            return;
        if (client.isRunning())
            throw new IllegalStateException("Cannot destroy running client!");
        else
            client = null;
    }

    @Override
    public ResourceType<Resource<Font>> getFontResourceType() {
        return FontResource.TYPE;
    }

    @Override
    public ResourceType<Resource<Font>> getPropertiedFontResourceType() {
        return PropertiedFontResource.TYPE;
    }
}