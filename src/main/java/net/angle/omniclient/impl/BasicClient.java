/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omniclient.impl;

import com.samrj.devil.gl.DGL;
import com.samrj.devil.gui.DUI;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.angle.omniresource.api.ResourceManager;

public class BasicClient extends AbstractClient {
    public BasicClient(ResourceManager resourceManager, String title) {
        super(resourceManager, title);
    }
    
    @Override
    public void init() {
        super.init();
        try {
            DUI.setFont(getResourceManager().load(FontResource.TYPE, "Helvetica-Normal.ttf", this));
        } catch (IOException ex) {
            Logger.getLogger(BasicClient.class.getName()).log(Level.SEVERE, "IOException while initializing DebugClient", ex);
        }
    }
    
    @Override
    public void destroy(Boolean crashed) {
        if (getScreen() != null)
            getScreen().destroy(crashed);
        
        getResourceManager().unloadResource("Helvetica-Normal.ttf", this);
        
        if (crashed) DGL.setDebugLeakTracking(false);
        
        DUI.destroy();
        DGL.destroy();
    }
}