/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omniclient.impl;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omniclient.api.Client;
import net.angle.omniclient.api.Screen;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public abstract class AbstractScreen<T extends Client> implements Screen<T> {
    private final @Getter T client;
}