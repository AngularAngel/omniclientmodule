/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omniclient.impl;

import com.samrj.devil.gui.Align;
import com.samrj.devil.gui.Button;
import com.samrj.devil.gui.DUI;
import com.samrj.devil.gui.Text;
import com.samrj.devil.gui.Window;
import java.util.function.Function;
import lombok.Getter;
import net.angle.omniclient.api.Client;
import net.angle.omniclient.api.MenuScreen;
import net.angle.omniclient.api.Screen;

/**
 *
 * @author angle
 */
public abstract class AbstractMenuScreen<T extends Client> extends AbstractScreen<T> implements MenuScreen<T> {
    protected @Getter Window titleWindow;
    protected @Getter Window menuWindow;

    public AbstractMenuScreen(T client) {
        super(client);
    }
    
    @Override
    public void populateTitleWindow(Text title) {
        titleWindow.setContent(title);
    }
    
    @Override
    public void createTitleWindow() {
        titleWindow = new Window();
        titleWindow.setTitleBarVisible(false);
        
        populateTitleWindow();
        
        DUI.show(titleWindow);
    }
    
    @Override
    public void createMenuWindow() {
        menuWindow = new Window();
        menuWindow.setTitleBarVisible(false);
        
        populateMenuWindow();
        
        DUI.show(menuWindow);
    }
    
    public Button getTransitionButton(String text, Function<T, Screen> constructor) {
        Button button = new Button(text);
        
        button.setCallback((t) -> {
            getClient().changeScreen(this, constructor.apply(getClient()));
        });
        return button;
    }
    
    public void alignWindows() {
        titleWindow.setPosAlignToViewport(Align.N.vector());
        menuWindow.setPosAlignToViewport(Align.S.vector());
    }

    @Override
    public void resize(int width, int height) {
        titleWindow.setSizeFromContent();
        menuWindow.setSizeFromContent();
        
        alignWindows();
    }

    @Override
    public void destroy(Boolean crashed) {
        if (titleWindow != null)
            DUI.hide(titleWindow);
        if (menuWindow != null)
            DUI.hide(menuWindow);
    }
}