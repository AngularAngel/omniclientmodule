/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.angle.omniclient.impl;

import com.samrj.devil.gui.Font;
import java.io.IOException;
import net.angle.omniresource.api.Resource;
import net.angle.omniresource.api.ResourceManager;
import net.angle.omniresource.api.ResourceType;
import net.angle.omniresource.impl.AbstractResource;
import net.angle.omniresource.impl.BasicResourceType;

/**
 *
 * @author angle
 */
public class FontResource extends AbstractResource<Font> {
    private static final String FONT_PATH = "fonts/";
    public static final ResourceType<Resource<Font>> TYPE = new BasicResourceType<>(FontResource::new, new String[]{"font", "ttf"});
    
    protected Font font;
    
    protected FontResource(String name) {
        super(name);
    }
    
    @Override
    public Font get() {
        if (!isLoaded()) throw new IllegalStateException();
        return font;
    }
    
    @Override
    public String getResourcePath() {
        return RESOURCE_PATH + FONT_PATH + getName();
    }
    
    @Override
    public void load(ResourceManager resourceManager) throws IOException {
        if (isLoaded()) throw new IllegalStateException();
        font = new Font(getInputStream());
    }

    @Override
    public void load(ResourceManager resourceManager, Class clazz) throws IOException {
        if (isLoaded()) throw new IllegalStateException();
        font = new Font(getInputStream(clazz));
    }
    
    @Override
    public void unload(ResourceManager resourceManager) {
        if (!isLoaded()) throw new IllegalStateException();
        font.destroy();
        font = null;
    }
    
    @Override
    public boolean isLoaded() {
        return font != null;
    }

    @Override
    public ResourceType<Resource<Font>> getType() {
        return TYPE;
    }
}