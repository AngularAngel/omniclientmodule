package net.angle.omniclient.impl;

import com.samrj.devil.gui.Button;
import com.samrj.devil.gui.LayoutColumns;
import com.samrj.devil.gui.TextField;
import java.util.Objects;
import java.util.function.Consumer;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author angle
 */
public class IntField extends LayoutColumns {
    private @Getter int value;
    private @Getter @Setter Consumer<IntField> onChanged = null;
    private TextField textfield = new TextField();

    public IntField(int startingValue) {
        super();
        Button button = new Button("-");
        
        button.setCallback((t) -> {
            setValue(--value);
        });
        
        add(button);
        
        button.setSizeFromText();
        
        textfield.setConfirmCallback((t) -> {
            try {
                setValue(Integer.parseInt(t.get()));
            } catch (Exception e) {
                setValue(value);
            }
        });
        
        add(textfield);
        
        button = new Button("+");
        
        button.setCallback((t) -> {
            setValue(++value);
        });
        
        add(button);
        
        button.setSizeFromText();
        
        setValue(startingValue);
    }
    
    public void setValueNoCallback(int newvalue) {
        this.value = newvalue;
        textfield.setText("" + value);
    }
    
    public void setValue(int newvalue) {
        setValueNoCallback(newvalue);
        if (Objects.nonNull(onChanged))
            onChanged.accept(this);
    }
}
