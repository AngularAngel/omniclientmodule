/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omniclient.impl;

import com.samrj.devil.game.GameWindow;
import com.samrj.devil.math.Vec2i;
import net.angle.omniclient.api.Client;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import net.angle.omniclient.api.Screen;
import net.angle.omniresource.api.ResourceManager;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public abstract class AbstractClient implements Client {
    private final @Getter ResourceManager resourceManager;
    private final @Getter String title;
    private @Getter Screen screen;
    private @Setter @Getter boolean running = false;
    private final @Getter Vec2i resolution = new Vec2i();
    
    @Override
    public void changeScreen(Screen newScreen) {
        if (screen != null)
            screen.destroy(false);
        screen = newScreen;
        screen.init();
    }

    @Override
    public void init() {
        resolution.set(GameWindow.getResolution());
    }
    
    @Override
    public void resize(int width, int height) {
        Client.super.resize(width, height);
        resolution.set(width, height);
    }
}