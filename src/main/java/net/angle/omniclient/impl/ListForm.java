package net.angle.omniclient.impl;


import com.samrj.devil.gui.Form;
import com.samrj.devil.gui.LayoutRows;
import com.samrj.devil.gui.ScrollBox;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


/**
 *
 * @author angle
 */
public class ListForm<T extends Form> extends ScrollBox {
    public final List<T> forms = new ArrayList<>();
    private final LayoutRows rows = new LayoutRows();
    private final Comparator<T> comparator;

    public ListForm(Comparator<T> comparator) {
        setContent(rows);
        this.comparator = comparator;
    }
    
    public void update() {
        rows.clear();
        forms.sort(comparator);
        forms.forEach((t) -> {
            rows.add(t);
        });
        updateSize();
    }
}