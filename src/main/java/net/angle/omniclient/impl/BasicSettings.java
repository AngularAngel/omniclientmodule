package net.angle.omniclient.impl;

import java.util.HashMap;
import java.util.Objects;
import java.util.function.Consumer;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import net.angle.omniclient.api.Settings;
import net.angle.omniclient.api.Settings.Setting;

/**
 *
 * @author angle
 */
public class BasicSettings implements Settings {
    
    private final HashMap<String, Setting> settings = new HashMap<>();
    
    @Override
    public <T> Setting<T> addSetting(Setting<T> setting) {
        settings.put(setting.getName(), setting);
        return setting;
    }
    
    @Override
    public <T> Setting<T> getSetting(String name, Class<T> type) {
        return settings.get(name);
    }
    
    @RequiredArgsConstructor
    public static class BasicSetting<T> implements Setting<T>{
        private final @Getter String name;
        private @Getter T value;
        private @Setter Consumer<T> callback;
        
        @Override
        public void setValue(T newValue) {
            if (!Objects.equals(value, newValue)) {
                value = newValue;
                if (callback != null)
                    callback.accept(newValue);
            }
        }
    }
}