/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package net.angle.omniclient.impl;

import com.samrj.devil.gui.Font;
import java.io.IOException;
import net.angle.omniresource.api.Resource;
import net.angle.omniresource.impl.JSONResource;
import net.angle.omniresource.api.ResourceManager;
import net.angle.omniresource.api.ResourceType;
import net.angle.omniresource.impl.AbstractResource;
import net.angle.omniresource.impl.BasicResourceType;
import pixelguys.json.JsonObject;

/**
 *
 * @author angle
 */
public class PropertiedFontResource extends AbstractResource<Font> {
    private static final String FONT_PATH = "fonts/";
    
    public static final ResourceType<Resource<Font>> TYPE = new BasicResourceType<>(PropertiedFontResource::new, new String[]{""});
    
    protected Font font;
    
    protected PropertiedFontResource(String name) {
        super(name);
    }
    
    @Override
    public boolean isLoaded() {
        return font != null;
    }
    
    @Override
    public Font get() {
        if (!isLoaded()) throw new IllegalStateException();
        return font;
    }
    
    @Override
    public String getResourcePath() {
        return RESOURCE_PATH + FONT_PATH + getName();
    }
    
    protected String propertiesFileName() {
        return getName() + "_properties.json";
    }
    
    protected Font.FontProperties getFontProperties(JsonObject json) {
        Font.FontProperties properties = new Font.FontProperties();
        properties.bitmapWidth = json.getInt("bitmapWidth", properties.bitmapWidth);
        properties.bitmapHeight = json.getInt("bitmapHeight", properties.bitmapHeight);
        properties.height = json.getFloat("height", properties.height);
        properties.first = json.getInt("first", properties.first);
        properties.count = json.getInt("count", properties.count);
        return properties;
    }

    @Override
    public void load(ResourceManager resourceManager) throws IOException {
        if (isLoaded()) throw new IllegalStateException();
        Resource<JsonObject> jsonResource = resourceManager.loadResource(JSONResource.TYPE, propertiesFileName(), this);
        JsonObject json = jsonResource.get();
        Resource<Font> fontResource = resourceManager.loadResource(FontResource.TYPE, json.getString("font", ""), this);
        Font.FontProperties properties = getFontProperties(json);
        font = new Font(fontResource.getInputStream(), properties);
        resourceManager.unloadResource(jsonResource.getName(), this);
        resourceManager.unloadResource(fontResource.getName(), this);
    }

    @Override
    public void load(ResourceManager resourceManager, Class clazz) throws IOException {
        if (isLoaded()) throw new IllegalStateException();
        Resource<JsonObject> jsonResource = resourceManager.loadResource(JSONResource.TYPE, propertiesFileName(), this, clazz);
        JsonObject json = jsonResource.get();
        Resource<Font> fontResource = resourceManager.loadResource(FontResource.TYPE, json.getString("font", ""), this, clazz);
        Font.FontProperties properties = getFontProperties(json);
        font = new Font(fontResource.getInputStream(), properties);
        resourceManager.unloadResource(jsonResource.getName(), this);
        resourceManager.unloadResource(fontResource.getName(), this);
    }
    
    @Override
    public void unload(ResourceManager resourceManager) {
        if (!isLoaded()) throw new IllegalStateException();
        font.destroy();
        font = null;
    }

    @Override
    public ResourceType<Resource<Font>> getType() {
        return TYPE;
    }
}